package com

object TicTacToe {
  class Game {
    val playerA = 'X'
    val playerB = 'O'
    val n = 3
    val mat = Array.ofDim[Char](n, n).map(r => r.map(_ => ' '))
    var set = 0

    def display = {
      for (i <- 0 until n) {
        println(mat(i).mkString(" | "))
        if (i < n - 1) println((0 until n + 2).map(_ => '-').mkString("-"))
      }
      println()
    }

    def play(player: Char, i: Int, j: Int) = {
      set = set + 1
      val row = i - 1
      val col = j - 1

      mat(row)(col) = player
      val won = checkWin(row, col, player)
      won match {
        case true => println(s"Winner is Player: player${player}")
        case _ =>
      }
      won
    }

    def checkWin(row: Int, col: Int, player: Char) = {
      val isColumnWin = columnCheck(col, player)
      val isRowWin = rowCheck(row, player)
      val isRightDiagonal = rightDiagonal(row, col, player)
      val isLeftDiagonal = leftDiagonal(row, col, player)

      isColumnWin || isRowWin || isRightDiagonal || isLeftDiagonal
    }

    def leftDiagonal(r: Int, c: Int, player: Char) = {
      var row = r
      var col = c
      var count = 0
      while (row >= 0 && col < n && mat(row)(col) == player) {
        row = row - 1
        col = col + 1
        count = count + 1
      }

      row = r + 1
      col = c - 1
      while (row < n && col >= 0 && mat(row)(col) == player) {
        row = row + 1
        col = col - 1
        count = count + 1
      }
      count match {
        case 3 => true
        case _ => false
      }
    }

    def rightDiagonal(r: Int, c: Int, player: Char) = {
      var row = r
      var col = c
      var count = 0
      while (row < n && col < n && mat(row)(col) == player) {
        row = row + 1
        col = col + 1
        count = count + 1
      }

      row = r - 1
      col = c - 1
      while (row >= 0 && col >= 0 && mat(row)(col) == player) {
        row = row - 1
        col = col - 1
        count = count + 1
      }
      count match {
        case 3 => true
        case _ => false
      }
    }

    def columnCheck(col: Int, player: Char): Boolean = {
      for (i <- 0 until n) {
        if(mat(i)(col) != player) {
          return false
        }
      }
      true
    }

    def rowCheck(row: Int, player: Char): Boolean = {
      for (i <- 0 until n) {
        if(mat(row)(i) != player) {
          return false
        }
      }
      true
    }

    def notDraw: Boolean = set != 9
    def draw = set == 9
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val game = new Game()
    game.display

    var done = false
    var player = game.playerA
    while (game.notDraw && !done) {
      println(s"player${player} turn:")
      val (row, col) = (scan.nextInt(), scan.nextInt())

      done = game.play(player, row, col)

      game.display

      player match {
        case game.playerA => player = game.playerB
        case game.playerB => player = game.playerA
      }
    }
    if (game.draw) println("Game is Draw")
  }
}

