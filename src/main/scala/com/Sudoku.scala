package com

import scala.collection.mutable

object Sudoku {

  class Matrix {
    val n = 9
    val sum = n * (n + 1) / 2
    val mat = Array.ofDim[Int](n, n)

    val zeros = new mutable.ListBuffer[(Int, Int)]()
    val rowMap = (0 until n).map(r => (r, mutable.Set[Int]())).toMap
    val colMap = (0 until n).map(r => (r, mutable.Set[Int]())).toMap
    val matMap = mutable.Map[(Int, Int), mutable.Set[Int]]()

    for (i <- 0 to 2) {
      for (j <- 0 to 2) {
        matMap.put((i * 3, j * 3), mutable.Set())
      }
    }

    def solve = {
      var iteration = 0
      while (zeros.nonEmpty) {
        println(s"iteration: ${iteration}")

        for (ij <- zeros) {
          val row = ij._1 / 3 * 3
          val col = ij._2 / 3 * 3
          val done = rowMap(ij._1).union(colMap(ij._2)).union(matMap((row, col)))
          if (done.size == n - 1) {
            val remainder = sum - done.sum
            mat(ij._1)(ij._2) = remainder
            zeros -= ij
            rowMap(ij._1).add(remainder)
            colMap(ij._2).add(remainder)
            matMap((row, col)).add(remainder)
          }
        }
        display
        iteration = iteration + 1
      }
    }

    def readData(scan: java.util.Scanner) = {
      for (i <- 0 until n) {
        for (j <- 0 until n) {
          val in = scan.nextInt()
          mat(i)(j) = in
          if (in == 0) zeros += ((i, j))
          if (in != 0) rowMap(i).add(in)
          if (in != 0) colMap(j).add(in)
          if (in != 0) {
            val row = i / 3 * 3
            val column = j / 3 * 3
            matMap((row, column)).add(in)
          }
        }
      }
    }

    def display = println(mat.map(r => r.mkString(" ")).mkString("\n"))
  }

  def main(args: Array[String]) = {
    val scan = new java.util.Scanner(System.in)
    val sudoku = new Matrix()
    sudoku.readData(scan)
    sudoku.solve
  }
}

/*
Easy
2 8 0 0 0 5 6 4 1
7 4 5 0 0 8 0 0 9
3 0 1 2 0 9 0 0 0
1 7 3 0 0 0 4 0 0
6 2 0 0 0 0 5 0 0
9 5 0 4 7 0 0 0 0
8 0 2 0 1 0 0 5 6
4 0 6 0 0 7 0 3 0
0 3 0 0 9 0 0 8 0

Medium
0 9 0 6 0 0 5 8 0
0 0 0 0 0 0 0 0 0
0 1 0 5 4 9 2 6 0
0 0 2 0 0 5 0 0 0
0 0 9 0 3 0 7 0 0
5 0 7 0 0 0 0 4 0
2 0 0 3 6 1 0 0 7
3 0 0 9 0 8 1 0 0
0 0 0 4 2 0 0 0 8

Hard
0 0 5 0 0 0 8 0 0
6 0 4 0 0 1 0 0 0
0 0 0 2 8 3 0 0 0
0 0 2 0 9 0 1 7 0
9 0 0 8 0 7 0 0 0
0 0 0 5 0 0 6 0 3
0 0 0 0 0 0 4 0 2
0 2 8 3 6 0 0 0 1
0 0 0 9 2 0 0 0 0
 */
