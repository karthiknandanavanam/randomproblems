package com

import scala.collection.mutable

/*
Cache only top n items

5 10
www.google.com 19.09.87.34
www.abc.com 19.09.87.34
www.bbc.com 19.09.87.34
www.cnn.com 19.09.87.34
www.item.com 19.09.87.34
www.amazon.com 19.09.87.34
www.pickle.com 19.09.87.34
www.dream.com 19.09.87.34
www.adobe.com 19.09.87.34
www.facebook.com 19.09.87.34

 */
object Caching {
  case class Cache(n: Int) {
    val map = mutable.Map[String, (String, Int)]()
    val history = mutable.ListBuffer[String]()

    def get(k: String) = {
      updateHistory(k)
      map(k)
    }

    def put(k: String, v: String) = {
      println(displayHistory)
      removeOldestAndAdd(k)
      map.put(k, (v, history.length - 1))
    }

    def removeOldestAndAdd(k: String) = {
      println(displayHistory)
      if (history.length >= n) {
        history.remove(0)
        history += k
      }
      else {
        history += k
      }
    }

    def updateHistory(k: String) = {
      map.get(k) match {
        case o if o.isDefined => history.remove(map(k)._2); history += k
        case _ => history += k
      }
    }

    def displayHistory = history.mkString(" ")
  }

  def main(args: Array[String]): Unit = {
    val scan = new java.util.Scanner(System.in)
    val maxCache = scan.nextInt()
    val size = scan.nextInt()

    val cache = Cache(maxCache)

    val arr = (0 until size).map(_ => (scan.next(), scan.next()))
    arr.foreach(r => cache.put(r._1, r._2))
    arr.map(r => cache.get(r._1))
  }
}
